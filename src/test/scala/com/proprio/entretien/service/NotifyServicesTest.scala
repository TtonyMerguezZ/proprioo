package com.proprio.entretien.service

import com.proprio.entretien.client.MailSender
import com.proprio.entretien.dto.BuyerModel.{Buyer, Criteria}
import com.proprio.entretien.repository.BuyersRepository
import com.proprio.entretien.utils.JsonSupport
import com.typesafe.config.ConfigFactory
import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{FlatSpecLike, Matchers}

class NotifyServicesTest extends FlatSpecLike with Matchers with JsonSupport with MockitoSugar {

  implicit val config = ConfigFactory.load()

  val mockRepo = mock[BuyersRepository]
  val mockMail = mock[MailSender]
  val notifyService = new NotifyService(mockRepo, mockMail)

    val criteria1 = Criteria("92600", 80, 3, false, false)
    val buyer1 = Buyer("Zinedine", "Zidane"," zz@real.com" ,criteria1)
    val criteria2 = Criteria("28500", 80, 2, true, false)
    val buyer2 = Buyer("Stephane", "Guivarch"," sg@auxerre.com" ,criteria2)

  val list = Seq(buyer1, buyer2)
  when(mockRepo.buyersList).thenReturn(list)

  "NotifyService" should "have one mail in return" in {
    val criteria = Criteria("92600", 56, 3, false, false)
    val retour = notifyService.extractPotentialMail(criteria)
    retour.size shouldBe 1
    retour(0) shouldBe buyer1.email
  }

  "NotifyService" should "have two mail in return" in {
    val criteria = Criteria("92600", 80, 3, true, false)
    val retour = notifyService.extractPotentialMail(criteria)
    retour.size shouldBe 2
  }

  "NotifyService" should "have zero mail in return" in {
    val criteria = Criteria("11111", 36, 1, true, true)
    val retour = notifyService.extractPotentialMail(criteria)
    retour.size shouldBe 0
  }

}
