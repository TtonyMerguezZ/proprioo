package com.proprio.entretien.repository

import java.io.FileNotFoundException

import com.proprio.entretien.utils.JsonSupport
import com.typesafe.config.{ConfigFactory, ConfigValueFactory}
import org.scalatest.{FlatSpecLike, Matchers}

//TODO Test both FileOpener and BuyerRepository (not that good :D) separate it
class BuyersRepositoryTest extends FlatSpecLike with Matchers with JsonSupport {

  "BuyerRepository" should "load the file correctly" in {
    val config = ConfigFactory.load()
    val buyersRepository = new BuyersRepository(config)
    buyersRepository.buyersList.size shouldBe 2
  }

  "BuyerRepository" should "throw an exception" in {
    val config = ConfigFactory.load().withValue("repository.buyers.file", ConfigValueFactory.fromAnyRef("wrong.json"))
    intercept[FileNotFoundException](new BuyersRepository(config))
  }
}
