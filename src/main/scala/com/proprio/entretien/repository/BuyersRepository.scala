package com.proprio.entretien.repository

import com.proprio.entretien.dto.BuyerModel.Buyer
import com.proprio.entretien.utils.{FileOpener, JsonSupport}
import com.typesafe.config.Config

import scala.util.{Failure, Success}

class BuyersRepository(config: Config) extends JsonSupport{
  val buyersList = {
    FileOpener.apply(config.getString("repository.buyers.file")) match {
      case Success(file) => mapper.readValue[Seq[Buyer]](file)
      case Failure(e) => throw e
    }
  }
}
