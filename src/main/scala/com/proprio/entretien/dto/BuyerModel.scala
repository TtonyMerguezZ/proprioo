package com.proprio.entretien.dto

object BuyerModel {

  /**
    * Simplified Buyer
    * @param firstName
    * @param lastName
    * @param email
    * @param criteria
    */
  case class Buyer(firstName: String, lastName: String, email : String, criteria: Criteria){
    require(firstName != null, "Firstname should not be null")
    require(email != null, "email should not be null")
    require(lastName != null, "lastname should not be null")
    require(criteria != null, "criteria should not be null")
  }

  /**
    * Simplified criteria
    * @param postCode
    * @param area
    * @param roomNb
    * @param balcony
    * @param parking
    */
  case class Criteria(postCode : String, area: Int, roomNb: Int, balcony: Boolean, parking: Boolean){
    require(postCode != null, "Postcode should not be null")
    require(area != null, "Area should not be null")
    require(roomNb!= null, "RoomNb should not be null")
    require(balcony != null, "Balcony should not be null")
    require(parking != null, "Parking should not be null")
  }
}
