package com.proprio.entretien.service

import com.proprio.entretien.client.MailSender
import com.proprio.entretien.dto.BuyerModel.{Buyer, Criteria}
import com.proprio.entretien.repository.BuyersRepository
import com.typesafe.config.Config

class NotifyService(val buyersRepository: BuyersRepository, val mailSender: MailSender)(implicit val config: Config) {

  private val nbCriteria = config.getInt("notification.nb-criteria ")

  private implicit def bool2int(b: Boolean) = if (b) 1 else 0

  private def findPotentials(buyer: Buyer, criteria: Criteria) : Option[Buyer] = {
    val nbGoodCriteria =(
      buyer.criteria.roomNb.equals(criteria.roomNb).toInt
    + buyer.criteria.postCode.equals(criteria.postCode).toInt
    + buyer.criteria.area.equals(criteria.area).toInt
    + buyer.criteria.balcony.equals(criteria.balcony).toInt
    + buyer.criteria.parking.equals(criteria.parking).toInt)

    if(nbGoodCriteria >= nbCriteria) Option(buyer) else Option.empty
  }

  def getPotentialBuyers(criteria : Criteria) : Unit = {
    mailSender.send(extractPotentialMail(criteria))
  }
  //TODO should be private
  def extractPotentialMail(criteria: Criteria) : Seq[String] = {
    for {
      buyers <- buyersRepository.buyersList
      potentials <- findPotentials(buyers, criteria)
    } yield potentials.email
  }
}
