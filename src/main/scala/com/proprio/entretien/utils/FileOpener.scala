package com.proprio.entretien.utils

import java.io.{FileNotFoundException, InputStream}
import java.nio.file.{Files, Paths}

import scala.util.{Failure, Success, Try}

object FileOpener extends Logging {
  val DefaultBufSize = 2048

  def apply(filePathAsString: String): Try[InputStream] = {
    Option(getClass.getClassLoader.getResourceAsStream(filePathAsString)) match {
      case Some(inputStream) => Success(inputStream)
      case None =>
        val message = s"Unable to find ${filePathAsString} resource"
        logger.warn(message)
        Failure(new FileNotFoundException(message))
    }
  }
}


