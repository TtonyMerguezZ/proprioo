package com.proprio

import com.proprio.entretien.client.MailSender
import com.proprio.entretien.repository.BuyersRepository
import com.proprio.entretien.service.NotifyService
import com.typesafe.config.{Config, ConfigFactory}


class Main() {
  implicit val config: Config = ConfigFactory.load()
  val buyersRepository = new BuyersRepository(config)
  val mailSender = new MailSender
  val notifyService = new NotifyService(buyersRepository, mailSender)

  //TODO server/routes creation
}
